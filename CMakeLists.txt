project(extavy)
cmake_minimum_required (VERSION 2.8.8)

if (CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR ) 
  message (FATAL_ERROR
    "In-source builds are not allowed. Please clean your source tree and try again.")  
endif()


add_subdirectory (abc)
add_subdirectory (minisat)
add_subdirectory (glucose)
add_subdirectory (avy)
